set -ex

if [ ! -n "$1" ]; then
    VER=`git tag | grep -v "v" | tail -n 1 |  python3 -c "import sys; vers = sys.stdin.readline().strip().split('.'); print('.'.join([*vers[:2], str(int(vers[2])+1)]))"`
else
    VER="$1"
fi
git tag $VER
git push -u origin ${VER}

docker build -t crpi-we9x1w5w2de3wyr1.cn-guangzhou.personal.cr.aliyuncs.com/chenxun/demo_fe:$VER .
docker push crpi-we9x1w5w2de3wyr1.cn-guangzhou.personal.cr.aliyuncs.com/chenxun/demo_fe:$VER
echo crpi-we9x1w5w2de3wyr1.cn-guangzhou.personal.cr.aliyuncs.com/chenxun/demo_fe:$VER

